// Modules
import { useEffect, useState } from 'react';

// Styles
import './App.css';

function App() {
const [sort, setSort] = useState();
const [sizes, setSizes] = useState([]);
const [uniqueSizes, setUniqueSizes] = useState([]);

useEffect(() => {
  async function fetchMoviesJSON() {
    const response = await fetch('https://fasterthanlight.me/api/frontend-challenge');
    const sizes = await response.json();
    setSizes(sizes.cluster.variations);
  }
  fetchMoviesJSON();
}, [])

useEffect(() => {
  const uniqueSizes = new Set();

  const unique = sizes.filter(element => {
  const isDuplicate = uniqueSizes.has(element.frameSize);

  uniqueSizes.add(element.frameSize);

  if (!isDuplicate) {
    return true;
  }

  return false;
});
  if (unique) {
    setUniqueSizes(unique)
  }
}, [sizes])

useEffect(() => {
  if (sort) {
    const fsize = sizes.find(x => x.id == sort)
    const duplicateId = sizes.filter(x => x.size == fsize.size && x.frameSize == fsize.frameSize)
    const output = duplicateId.map(item => item.id)
    console.log('output', output);
  }
}, [sort]);

  return (
    <div className="App">
      <span>Pick a size:</span>
      <span>
        <select onChange={(e) => setSort(e.target.value)}>
            {uniqueSizes && uniqueSizes.map((size) => (
              <option key={size.id} value={size.id}>
                {size.size}|{size.frameSize}
              </option>
            ))}
        </select>
      </span>
    </div>
  );
}

export default App;
